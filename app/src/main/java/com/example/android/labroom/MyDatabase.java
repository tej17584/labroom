package com.example.android.labroom;

import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.RoomDatabase;

/**
 * Created by Jose Tejada on 1/3/2018.
 */
@Database(entities = {Product.class}, version = 1)
public abstract class MyDatabase extends RoomDatabase {
    public abstract ProductDao productDao();
}
